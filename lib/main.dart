import 'package:flutter/material.dart';
import 'package:todo_app/ui/screens/main_screen/main_screen.dart';
import 'package:todo_app/ui/screens/splash_screen.dart';
import 'package:todo_app/ui/screens/user_type_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      fontFamily: 'RobotoSlab'
    ),
    home: MainScreen(),
  );
}
