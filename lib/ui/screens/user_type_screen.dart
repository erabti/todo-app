import 'package:flutter/material.dart';
import 'package:todo_app/ui/widgets/button.dart';
import 'package:todo_app/ui/widgets/logo.dart';

class UserTypeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        body: Column(
          children: <Widget>[
            Logo(),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    "What's your role at school?",
                    style: TextStyle(
                      fontSize: 25  ,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Button(
                    text: "Student",
                    onPressed: () {},
                  ),
                  Button(
                    text: "Instructor",
                    onPressed: () {},
                  ),
                ],
              ),
            ),
          ],
        ),
      );
}
