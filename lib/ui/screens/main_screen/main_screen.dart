import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo_app/ui/screens/main_screen/widgets/custom_drawer.dart';
import 'package:todo_app/ui/screens/main_screen/widgets/sheet.dart';

enum Priority {
  urgent,
  important,
  medium,
  useless,
}

class DummyTask {
  final String text;
  bool isChecked;
  final Priority priority;

  Color getColor() => {
        Priority.useless: Colors.grey,
        Priority.medium: Colors.green,
        Priority.important: Colors.yellow,
        Priority.urgent: Colors.red,
      }[this.priority];

  DummyTask({
    @required this.text,
    this.isChecked = false,
    this.priority = Priority.medium,
  });
}

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final tasks = [
    DummyTask(
        text:
            "Dlkjdsfdsf sdf lkjaf lksaj flksjadf lkasjflkjsadf klasjdfkl asjdflk jasldkfjalskdfjlksjdflk jadsk lj"),
    DummyTask(text: "sdfxc sdf", isChecked: true),
    DummyTask(text: "qwe sdf", isChecked: true, priority: Priority.urgent),
    DummyTask(text: "asd sdf", priority: Priority.useless),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () => showModalBottomSheet(
            context: context,
            builder: (context) => Sheet(),
            isScrollControlled: true,
            useRootNavigator: true
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        ),
        drawer: CustomDrawer(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        appBar: AppBar(
          title: Text("To-Done"),
          centerTitle: true,
        ),
        body: ReorderableListView(
          onReorder: (oldIndex, newIndex) {},
          children: [
            ...tasks.map(
              (task) => Card(
                key: Key(task.text),
                elevation: 4,
                child: ListTile(
                  title: Text(
                    task.text,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  trailing: LayoutBuilder(builder: (context, constraints) {
                    return SizedBox(
                      height: constraints.maxHeight,
                      width: constraints.maxWidth * 0.1,
                      child: RotatedBox(
                        quarterTurns: 1,
                        child: Icon(
                          Icons.bookmark,
                          size: constraints.biggest.height,
                          color: task.getColor(),
                        ),
                      ),
                    );
                  }),
                  leading: Checkbox(
                    onChanged: (n) {},
                    value: task.isChecked,
                  ),
                ),
              ),
            ),
          ],
        ));
  }
}
