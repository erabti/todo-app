import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo_app/ui/widgets/logo.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) => SizedBox(
        width: MediaQuery.of(context).size.width * 0.5,
        child: Drawer(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                DrawerHeader(
                  padding: EdgeInsets.zero,
                  child: Logo(),
                ),
                Container(
                  child: Text(
                    "Student",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.blue.shade100,
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.3,
                ),
                DecoratedBox(
                  decoration: BoxDecoration(color: Colors.blue.shade50),
                  child: ListTile(
                    title: Text(
                      "Feedback",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                DecoratedBox(
                  decoration: BoxDecoration(color: Colors.blue.shade50),
                  child: ListTile(
                    title: Text(
                      "Contact Us",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                DecoratedBox(
                  decoration: BoxDecoration(color: Colors.blue.shade50),
                  child: ListTile(
                    title: Text(
                      "Log out",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
